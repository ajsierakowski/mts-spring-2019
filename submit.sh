#!/bin/bash
#SBATCH --job-name=MyJob
#SBATCH --time=0:10:0
#SBATCH --partition=debug
#SBATCH --gres=gpu:1
#SBATCH --nodes=1
# number of tasks (processes) per node
#SBATCH --ntasks-per-node=1
#SBATCH --mail-type=end
#SBATCH --mail-user=asierak1@jhu.edu

#### load and unload modules you may need
# module unload openmpi/intel
# module load mvapich2/gcc/64/2.0b
module list

sleep 10

#### execute code and write output file to OUT-24log.
# time mpiexec ./code-mvapich.x > OUT-24log
echo "Finished with job $SLURM_JOBID"
