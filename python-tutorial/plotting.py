#!/usr/bin/env python3

# import modules
import numpy as np
import matplotlib.pyplot as plt

# use numpy to create dataset
N = 100
x = np.zeros(N)
y = np.zeros(N)

for i in range(N):
    x[i] = i*2.*np.pi/(N-1)
    y[i] = np.sin(x[i])

# plot with matplotlib
plt.xlabel('x')
plt.ylabel('sin(x)')
plt.title('y = sin(x)')

plt.axis([0, 2.*np.pi, -1, 1])

plt.plot(x,y)

# save the plot to a file
#fig = plt.gcf()
#fig.get_size_inches(5,4)
#fig.savefig('plot.png')

# show the plot on the screen
plt.show()
