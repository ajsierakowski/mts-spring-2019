#!/usr/bin/env python3

# numbers
a = 123
b = 456
c = a + b
print(c)

d = 1.5
e = 4
f = d * e
print(f)

g = 2
h = 8
i = g ** h
print(i)
