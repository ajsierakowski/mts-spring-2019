#!/bin/bash

#SBATCH --partition=debug
#SBATCH --time=0:30:0
#SBATCH --nodes=1
#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=1

# load modules
ml python/3.7

which python3

# enter virtual environment
source /home-4/t-asierak1@jhu.edu/scratch/mts-spring-2019/python-tutorial/my_environment/bin/activate

which python3

./hello.py
