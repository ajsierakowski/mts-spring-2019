# define the input query
def get_input():
    return input('Type something: ')

# determine whether this is a single character or a string
def check_length(string):
    if len(string) < 1:
        print('  Empty')
    elif len(string) < 2:
        print('  Character')
    else:
        print('  String')


