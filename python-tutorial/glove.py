#!/usr/bin/env python

import sys
import time

if len(sys.argv) < 2:
  print('usage: glove.py <search_word1> <search_word2>')
  exit(1)

search_words = sys.argv[1:]

print('...comparing the following words:')
for w in search_words:
  print('  %s' % w)

f = open('glove.6B.300d.txt', 'r')

start_time = time.time()

index = 0
found_words = []
for line in f:
  word = line.split()[0]
  index = index + 1
  if word in search_words:
    print('found in %f seconds (index %d)' % (time.time() - start_time, index))
    i = 0
    # move word from one list to the other
    for s in search_words:
      if word == s:
        found_words.append({'word': search_words.pop(i), 'numbers': line.split()[1:]})
      i = i + 1
    print(search_words, found_words)
    #print('fancy numbers:')
    #for n in line.split()[1:]:
    #  print('  %f' % (float(n)))
    #exit(0)

print('not found :-(')
exit(3)
