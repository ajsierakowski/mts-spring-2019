#!/usr/bin/env python3

# strings

H = 'Hopkins'
print(H)

print(len(H))
print(H[0])
print(H[6])
print(H[-1])
print(H[-2])
print(H[0:3])
print(H[3:])
print(H[:5])
print(H + ' University')
print(H * 4)
print(H.find('pki'))
print(H.find('pi'))
print(H.replace('op', 'ad'))
print(H)
JHU = 'Johns ' + H + ' University'
print(JHU)
