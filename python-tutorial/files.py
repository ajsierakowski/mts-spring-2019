#!/usr/bin/env python3

f = open('lists.py', 'r')
text = f.read() # reads all into ram at one time
print(text)

f.close()
# close and open to reset file
f = open('lists.py', 'r')

# reads one line into ram at a time
for line in f:
    print(line)

f.close()
