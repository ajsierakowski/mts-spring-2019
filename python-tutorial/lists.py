#!/usr/bin/env python3

L = ['H', 'o', 'p', 'k', 'i', 'n', 's']
print(L)
L[2] = 'P'
print(L)

# does not work for a string
#H = 'Hopkins'
#H[2] = 'P'
#print(H)

L = ['JHU', 1876, None]
print(L[2])
print(L[2] == True)
L[2] = True
print(L)

L.append('Baltimore')
print(L)
print(L.pop(2))
print(L)
#L.sort() # doesn't work because mixed types
L[1] = 'Homewood'
print(L)
L.sort()
print(L)
