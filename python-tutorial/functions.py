#!/usr/bin/env python3

# importing modules
# option 1
#import module_directory.my_module
# option 2
#import module_directory.my_module as mm
# option 3
from module_directory.my_module import get_input, check_length
# if module_directory is not in current directory, run this in the shell (or put in ~/.bashrc)
# export PYTHONPATH=/path/to/module_directory

# get user input
# option 1
#user_in = module_directory.my_module.get_input()
# option 2
#user_in = mm.get_input()
# option 3
user_in = get_input()

# repeat until a 'q' appears
while user_in.find('q') == -1:
    # determine whether this is a single character or a string
# option 1
#    module_directory.my_module.check_length(user_in)
# option 2
#    mm.check_length(user_in)
# option 3
    check_length(user_in)

    # request another input
# option 1
#    user_in = module_directory.my_module.get_input()
# option 2
#    user_in = mm.get_input()
# option 3
    user_in = get_input()

print('Exit')
