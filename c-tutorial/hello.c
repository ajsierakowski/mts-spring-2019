#include <stdlib.h>
#include <stdio.h>

#include "reader.h"

#include <factorial.h>

int main(int argc, char *argv[]) {
  printf("Hello world...\n");

  // parse the command line arguments
  printf("There are %d command line arguments.\n", argc);
  printf("They are:\n");
  for(int i = 0; i < argc; i++) {
    printf("  %d: %s\n", i, argv[i]);
  }

  // read a file
  float a = 0.;
  float b = 0.;
  float c = 0.;
  //read("/home-4/t-asierak1@jhu.edu/mts-spring-2019/c-tutorial/hello.conf", &a, &b, &c);
  read("hello.conf", &a, &b, &c);

  printf("a = %f\n", a);
  printf("b = %f\n", b);
  printf("c = %f\n", c);

  // calculate factorial
  printf("5! = %d\n", factorial(5));

  return EXIT_SUCCESS;
}
