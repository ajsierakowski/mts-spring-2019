#include <stdio.h>

void read(char *fname, float *a, float *b, float *c) {
  // open file
  FILE *fin = fopen(fname, "r");

  // read file
  fscanf(fin, "a = %f\n", a);
  fscanf(fin, "b = %f\n", b);
  fscanf(fin, "c = %f\n", c);

  // close file
  fclose(fin);
}
