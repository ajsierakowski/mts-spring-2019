set number
set printoptions=number:y,left:5pc,paper:letter
set shiftwidth=2
set tabstop=2
set expandtab
set scrolloff=15
set background=dark
set title
"set nowrap

"set relativenumber

let g:overlength_enabled = 1
highlight OverLength ctermbg=red ctermfg=white guibg=#592929
match OverLength /\%81v.\+/

function! ToggleOverLength()
    if g:overlength_enabled == 0
        match OverLength /\%81v.\+/
        let g:overlength_enabled = 1
        echo 'OverLength highlighting turned on'
    else
        match
        let g:overlength_enabled = 0
        echo 'OverLength highlighting turned off'
    endif
endfunction

nnoremap <leader>h :call ToggleOverLength()<CR>

" Turn off parenthesis matching for LaTeX files to prevent lag
autocmd FileType tex :NoMatchParen

" use real tabs in Makefiles
autocmd BufEnter ?akefile* set noet ts=8 sw=8

" set make program fro LaTeX files
autocmd FileType tex setlocal makeprg=pdflatex\ '%'

"fun! Count80()
  "au BufWinEnter * let w:m1=matchadd('Search', '\%<81v.\%>77v', -1)
  "au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
"endfun

"autocmd BufWritePre * call Count80()
"autocmd FileType tex let b:Count80=1
