#!/bin/bash

# read command line arguments

echo $0
echo $1
echo $2

echo "The number of args is: $#"

echo "All of the args are: $@"

for arg in $@
do
  echo $arg
done

# provide usage to user if used improperly
if [ $# -gt 0 ]
then
  echo create directories named $1
else
  echo args.sh usage: args.sh directory_name
fi

echo $PATH
echo
echo $USER
echo
echo $RANDOM
