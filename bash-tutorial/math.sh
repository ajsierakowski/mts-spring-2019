#!/bin/bash

let a=30/2

# note that bash does integer math only
echo $a

b=$((5+10))
echo $b

#a='hello'
#b='hello'

# string comparison 
#if [ $a == $b ]
# integer comparison
if [ $a -eq $b ]
then
  echo equal
else
  echo not equal
fi

# doesn't work as expected
#if [ $a > $b ]
# use this
if [ $a -gt $b ]
then
  echo a greater than b
else
  echo a not greater than b
fi

function print_hello {
  echo HELLO!
}

print_hello
