#!/bin/bash

a=Hello
b=World

echo $a $b
echo

# use variable
dir=/home-4/t-asierak1@jhu.edu/
contents=$(ls $dir)

# broken; I don't know how to do this
#echo $contents
#cd $contents[0]
#pwd

# save contents of command in variable
dir2=$(pwd)
echo $dir2

# no quotes read only next word

# single quotes are literal
c='Hi mom "$dir2"'
echo $c

# double quotes substitute variables
d="Hi mom \"$dir2\""
echo $d

D="Hi dad"
echo $d
echo $D
