#!/bin/bash
#SBATCH --job-name=MyJob
#SBATCH --time=0:10:0
#SBATCH --partition=debug
##SBATCH --gres=gpu:1
#SBATCH --nodes=1
# number of tasks (processes) per node
#SBATCH --ntasks-per-node=1
##SBATCH --mail-type=end
##SBATCH --mail-user=asierak1@jhu.edu

#### load and unload modules you may need
module list

sleep 10

echo "This is array number $SLURM_ARRAY_TASK_ID"

echo "Finished with job $SLURM_JOBID"
