#!/bin/bash

exp=experiment

echo Hello World!

# create parent directory
if [ -d ${exp}s ]
# if directory exists
then
  echo ${exp}s exists
else
  mkdir ${exp}s
fi

cd ${exp}s

# create experiment directories
for I in {1..10}
do
  if [ -d ${exp}_$I ]
  then
    echo ${exp}_$I exists
  else
    echo creating ${exp}_$I
    mkdir ${exp}_$I
  fi

  # create file with experiment index in it
  cd ${exp}_$I
  # if use ">": replace file
  # if use ">>": append to file
  echo more stuff experiment number $I >> file
  cd ..
done
