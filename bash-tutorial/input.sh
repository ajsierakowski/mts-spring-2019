#!/bin/bash

# ask for directory name
echo Hi. What should we call these directories?

read dir_name

echo Creating directories $dir_name

# use this variable to create new directories like in hello.sh

read -p 'Are you sure? ' sure
echo $sure
read -sp 'Are you secretly sure? ' sure_secret
echo
echo $sure_secret
